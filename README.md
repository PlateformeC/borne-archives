# Borne d'archives

### TL;DR : Archivage des sites inactifs en local dans des versions statiques sur une Raspberry Pi 4 sous Raspbian ou UbuntuCore

## Pourquoi une borne d'archives pour les sites de PiNG ?

En 2019, pour affiner notre stratégie de valorisation et de partage de ressource, nous avons été accompagné par l’agence Opixido. Nous avons fait l'inventaire de l'ensemble des contenus postés par PiNG sur le web depuis plus de 15 ans. C'était... vertigineux. 
Comme le soulignait notre consultante, il est vrai que nous avions eu une fâcheuse tendance à expérimenter chaque nouvelle possibilité du web, chaque nouveau CMS, chaque nouvelle plateforme de blogs. La farandole des innovations, résultait aujourd'hui une liste d'une quizaine de sites internets fragilisés, aux technologies disparates voire obsolètes. 

De cet inventaire, est ressortie la volonté de rationaliser notre mise en ligne d’informations dans une logique de sobriété numérique : Après plusieurs années d’exploration du concept d’anthropocène, nous ne souhaitons pas que notre production de contenus soit elle-même surconsommatrice de ressources environnementales.

C'est pourquoi nous avons souhaité "ramasser nos poubelles du web". Il s'agissait de trouver le moyen de supprimer du réseau internet nos plus anciens sites, tout en conservant une mémoire de cette présence passée en ligne.
Pour assurer cet archivage, une solution commune (et très impressionnante) est la [Wayback Machine](https://web.archive.org/web/20160314095815/http://www.pingbase.net/) développée par [Internet Archive](https://archive.org/). Ce site lancé en 1996 stocke et indexe des clichés du web et permet à ses utilisateurs de voir les versions archivées de pages Web à travers le temps. Cependant, l'archivage effectué n'est accessible qu'en ligne (bien que des sauvegardes soient disséminées de part le monde, à la Bibliotheca Alexandrina notamment, à Alexandrie).
À l'inverse, nous voulions retirer nos archives du web, ce qui nécessitait la mise à disposition d'un stockage physique, local. L'idée nous est donc venue de créer une borne, mise à disposition dans l'espace ressource de l'association et permettant de consulter les anciens sites internets de PiNG. 

Dans une perspective ethnologique, il était originellement envisagé de recréer au possible l'environnement de consultation de chaque site internet à l'aide de plusieurs machines virtuelles utilisant les systèmes d'exploitation correspondants à l'époque de chaque site. Cependant cette solution nous est rapidement parue trop complexe à mettre en place compte tenu des besoins réels de consultation de ces  sites et des moyens techniques que nous pouvions déployer.

Nous avons donc fait le choix d'une solution "low-tech" et faite main : Une borne réalisée à partir d'une carte électronique Raspberry Pi.

##### TO-DO
:::info
- [ ]Borne d'archive : 
    - [ ] Nettoyage du code des sites aspirés pour la borne :
        - [x] renum.net/leblog
        - [x]festivald.net/2014
        - [x]festivald.net/2015
        - [x]plateforme-c.org
        - [x]parcoursnumeriques.net
        - [x]artlabo.org/0 
        - [x]artlabo.org/1 
        - [x]labtolab.org 
        - [x]parcoursnumeriques.net/carte
        - [x]summerlab 2012
        - [x]summerlab 2013
        - [x]summerlab 2014 (liens à corriger)
        - [x]summerlab 2016 (liens à corriger)
        - [ ]marsmultimedia.info (à réaspirer)
        - [ ]renum.net (cassé)
    - [x] Préparation des plans
    - [x] Finalisation du design
    - [/] Écrire un article fablabo
:::

**Besoins - budget estimé :**

Autour de 200€ selon si l'on peut réutiliser une carte électronique et du matériel existant ou s'il faut tout acheter ?

- Une Raspberry Pi 3B+ ou 4 37,95€
    - Rpi 4 2G sur [kubii](https://www.kubii.fr/cartes-raspberry-pi/2771-nouveau-raspberry-pi-4-modele-b-2gb-0765756931175.html) promo 38,50€
    - Adaptateur [µHDMI -> VGA](https://www.amazon.fr/CSL-Adaptateur-transmission-conversion-analogique/dp/B07G819S6M/ref=sr_1_3?keywords=adaptateur+micro+hdmi+vga&qid=1584102588&sr=8-3) {lien amazon déso}
- Raspberry Pi 3B+ clear case 6€
    - Case en laser (adrien) customisable avec support VESA (si dispo sur écran de récup)
- Une carte SD 32go 10-20€
- Un chargeur USB (avec interrupteur ?) 9,90€
    - Tu arrêtes le RPi : sudo shutdown -h now ou avec le menu graphique
tu le redémarres en court-circuitant les bornes "run" (=reset) || prévoir bouton momentané
[source](https://www.raspberrypi.org/forums/viewtopic.php?p=1228394&sid=f1ded7c5b108451884247587c5237edf#p1228394) 
- Un câble HDMI
- Un clavier [avec pad intégré ex: logitech k400](https://www.logitech.fr/fr-fr/product/wireless-touch-keyboard-k400-plus?crid=27) 30-40€
    - Clavier [kubii](https://www.kubii.fr/claviers-et-peripheriques-usb/2782-rii-k18rgb-3272496299870.html) {avec fil !!}
- Un écran de récup' à PFC ([ou tactile DIY](https://fr.aliexpress.com/popular/infrared-touch-frame.html) stylé mais overkill)
- Haut-parleurs de récup ?
- Panneaux de CP 5mm pour [habillage](https://www.instructables.com/id/2-Player-Bartop-Arcade-Machine-Powered-by-Pi/) 40€

- système d'alimentation :
    - 1 prise elec M iec c14 montage panneaux 
    - 2 prises elec int. montage en saillie (Rpi + ecran) 
- casque de consultation audio (prise jack en facade / passe cable ) ?
- Bouton momentané 

Le temps de la réalisation :
- Un câble RJ45
- Une souris
- Peinture
- Colle à bois
- Vis
- Petit outillage atelier
- Instruments de découpe
- Serre-joints

**En images:**

![](https://i.imgur.com/XROPZCP.png)
![](https://i.imgur.com/YDThAiD.png)
![](https://i.imgur.com/gBr1CJm.jpg)


##### Questions à traiter lors de la suppression de ces sites du web : 
Faut-il faire pointer les noms de domaines sur la recherche la plus approchante ou sur pingbase.net ? 


### Ressources techniques :

##### Méthode pour convertir un site en statique :

Tout type de sites : https://www.httrack.com/html/shelldoc.html
Wordpress : https://wordpress.org/plugins/static-html-output-plugin/

##### Commande pour créer un lanceur sur Ubuntu :
```
gnome-desktop-item-edit ~/Bureau/ --create-new
```
##### Créer un kiosque :
https://forum.ubuntu-fr.org/viewtopic.php?id=227580

Sur raspbian : https://www.jjtronics.com/wordpress/2016/11/04/raspberry-pi-raspbian-pixel-et-chromium-en-mode-kiosk-ouverture-automatique-dun-navigateur-en-mode-plein-ecran-au-demarrage-de-raspbian/




MESURES : 
Écran VESA 100x100 trous de 5mm
Profondeur d'éran de 67mm à prévoir
Mesures clavier : 325 x 121
Mesures écran : 
- Intérieur : 330 largeur x 269 hauteur
- Extérieur : 374 largeur x 330 hauteur
- Hauteur pied compris : 393 
SVG 

Générateur de plans de bornes d'arcade : https://www.festi.info/boxes.py/Arcade?language=fr